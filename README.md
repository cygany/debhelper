That is a port of debhelper for dpkg in cygwin, also for the
current project Cygany.

```
origin	 git@gitlab.com:cygany/debhelper.git (fetch)
origin	 git@gitlab.com:cygany/debhelper.git (push)
upstream https://salsa.debian.org/debian/debhelper.git (fetch)
upstream https://salsa.debian.org/debian/debhelper.git (push)
```

Our branches are set up to follow the debian upstream:

```
* cygwin                                                  bf647306 [origin/cygwin] mostly temporarily changes on bootstrap pur
pose
  main                                                    fedaeeb9 [upstream/main] Release debhelper/13.11.9
```

Compile with: (dpkg must be already in path):

```
debian/rules build
PERL5LIB=/usr/share/perl5 DEB_RULES_REQUIRES_ROOT=no debian/rules binary
cd .. && dpkg -i libdebhelper-perl* debhelper*
```
